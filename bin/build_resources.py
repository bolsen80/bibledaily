"""
Parse lines like this:

2:6 But there went up a mist from the earth, and watered the whole
face of the ground.

2:7 And the LORD God formed man of the dust of the ground, and
breathed into his nostrils the breath of life; and man became a living
soul.

2:8 And the LORD God planted a garden eastward in Eden; and there he
put the man whom he had formed.
"""

import re
import sys
import os
import sqlite3
import requests


from typing import List

def retrieve_kjv() -> int:
    """
    Gets the kjv file from project gutenberg. Returns an error code
    suitable for sys.exit().
    """
    if os.path.exists('data/pg10.txt'):
        print("File already exists", file=sys.stderr)
        return 1

    data = requests.get('https://gutenberg.org/cache/epub/10/pg10.txt')
    with open('data/pg10.txt', 'w') as bible_file:
        bible_file.write(data.read)

    return 0

def apply_kjv_diff():
    os.chdir('data')
    result = os.system('patch -b pg10.txt kjv.diff')
    os.chdir('..')
    return result


def parse_verse(verse: str) -> str:
    """
    A verse is defined as:
    Line -> <Chapter>:<Verse> <Text> "\n"
    Chapter -> Number
    Verse -> Number
    Text -> String
    """

    try:
        chapter_verse = re.findall(r'^(\d+:\d+)\s', verse)[0]
        content = verse.replace(chapter_verse, '')
        final_line = chapter_verse.split(':')
        final_line.append(content.strip().replace('\n', ' '))
    except:
        return ['+', verse]
    return final_line

def parse_verses(bible_book: str):
    verses: List[str] = []
    parsed_lines = re.split(r'\n\n', bible_book)
    for line in parsed_lines:
        verse_line = parse_verse(line)

        # this bit is a hack because there is a bunch of lines
        # that are part of a verse but it was formatted oddly.
        if verse_line[0] == "+":
            last_verse: str = verses[-1][-1]
            new_verse_line: str = verse_line[1]
            last_verse_appended = last_verse + new_verse_line
            verses.append(last_verse_appended)
        else:
            verses.append(verse_line)

    return verses

def parse_gutenberg_kjv(bible: str):
    """
    The data itself is cleaned up to just the text.

    The bible file has the structure of
    <Bible> -> <Book>
    <Book> -> <Title> "\n"{2} <Verses> "\n"{4}
    <Verses> -> <Verse> (<Verse>)+?
    <Verse> -> (look at parse_verse())
    """
    books = bible.split('\n\n\n\n')
    parsed = []
    for book in books:
        book_split = book.split('\n\n\n')
        title = book_split[0].strip()
        text = book_split[1]
        parsed.append((title, parse_verses(text)))
    return parsed

def test_parse_gutenberg_kjv():
    with open('data/pg10.txt', 'r') as bible_file:
        data = bible_file.read()
        results = parse_gutenberg_kjv(data)
        assert len(results) == 66

def parse_reading_plan(reading_plan: List[str]):
    """
    Each line in the file is:
    day #,one or more book/chapter/verse-range.

    The book/chapter/verse-range is in the form of in rough BNF form:
    Ref -> <Book> "\s" <Chapter> (":" <VerseRange>)?
    Book -> String
    Chapter -> Number
    VerseRange -> Number ("-" Number)?

    The return format is:
    [day, [[book, chapter, verse_range]]]
    """
    parsed_lines = []
    for plan_line in reading_plan:
        plan_line_list = plan_line.strip().split(',')
        day = plan_line_list[0]
        passages = plan_line_list[1:]
        parsed_passages = []

        # parse all the passage references
        for passage in passages:
            passage_list = passage.split(' ')
            book = passage_list[0]
            next_token = 1
            if len(passage_list) == 3: # that is, the name is like "1 Chronicles"
                book += f" {passage_list[1]}"
                next_token = 2

            chapter_verse = passage_list[next_token].split(':')
            chapter = chapter_verse[0]
            verse_part = chapter_verse[1] if len(chapter_verse) == 2 else None
            verses = None
            if verse_part:
                verse_range = verse_part.split('-')

                if len(verse_range) == 2:
                    verses = range(int(verse_part.split('-')[0]),
                               int(verse_part.split('-')[1]))
                else:
                    verses = range(int(verse_range[0]))
            parsed_passages.append([book, chapter, verses])
        parsed_lines.append([day] + parsed_passages)
    return parsed_lines

def test_parse_reading_plan():
    fixtures = [
        "212,1 Chronicles 9,1 Chronicles 10:1-14,Romans 14:1-18,Proverbs 18:17-24,Proverbs 19:1-2"
    ]
    assert parse_reading_plan(fixtures) == [['212', ['1 Chronicles', '9', None], ['1 Chronicles', '10', range(1,14)], ['Romans', '14', range(1, 18)], ['Proverbs', '18', range(17, 24)], ['Proverbs', '19', range(1, 2)]]]


def setup_db(name: str):
    con = sqlite3.connect(name)
    cur = con.cursor()
    with open('bible_data.sql', 'r', encoding='utf-8') as tables:
        cur.executescript(tables.read())


def setup_bible_data(name: str):
    con = sqlite3.connect(name)
    cur = con.cursor()

    # fill up the bible data
    with open('data/pg10.txt', 'r', encoding='utf-8') as bible_file:
        results = parse_gutenberg_kjv(bible_file.read())
        for result in results:
            title, verses = result
            for verse in verses:
                cur.execute("insert into verses (book, chapter, verse, verse_text) values(?, ?, ?, ?)", (title, verse[0], verse[1], verse[2]))


def setup_reading_plan_data(name: str):
    con = sqlite3.connect(name)
    cur = con.cursor()

    # fill up the reading plan
    with open('data/reading_plan.txt', 'r', encoding='utf-8') as reading_plan_file:
        results = parse_reading_plan(reading_plan_file.readlines())
        for result in results:
            day = result[0]
            verses = result[1:]
            for verse in verses:
                verse_tuple = tuple(filter(lambda a: a, [day] + verse))
                print(verse_tuple)
                if len(verse_tuple) == 3:
                    cur.execute("insert into reading_plan (day, book, chapter) values(?,?,?)", verse_tuple)
                else:
                    verse_start = verse_tuple[-1][0]
                    verse_stop = verse_tuple[-1][-1] + 1
                    verse_tuple_data = (day, verse_tuple[1], verse_tuple[2], verse_start, verse_stop)
                    cur.execute("insert into reading_plan (day, book, chapter, verse_start, verse_stop) values(?,?,?,?,?)", verse_tuple_data)

    con.commit()

if __name__ == '__main__':
    pass
