create table if not exists verses (
       id integer primary key autoincrement,
       book text not null,
       chapter integer not null,
       verse integer not null,
       verse_text text not null
);

create table if not exists reading_plan (
       id integer primary key autoincrement,
       day integer not null,
       book text not null,
       chapter integer not null,
       verse_start integer,
       verse_stop integer
)
