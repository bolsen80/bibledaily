from flask import Flask
from flask_cors import CORS
import sqlite3
import json

from typing import List

app = Flask(__name__)
CORS(app)

def render_verse(verse: List[str]):
    day, book, chapter, start, stop = verse;
    result = f"{book} {chapter}"
    if start and stop:
        result += f":{start}-{stop}"
    return result

@app.route('/verses/<int:day>')
def get_plan_day(day):
    con = sqlite3.connect('bible.db')
    cur = con.cursor()
    print(day)
    reading_plan_results = cur.execute('select day, book, chapter, verse_start, verse_stop from reading_plan where day = ?', (int(day),)).fetchall()
    found_verses = {}
    for result in reading_plan_results:
        [verse_day, book, chapter, start, stop] = result
        # TODO: The use of like '%book%' is because "Genesis" has some random unicode char that can't be replaced when building. This is something
        # to look into.
        if start is None and stop is None:
            found_verses[render_verse(result)] = cur.execute('select * from verses where book like ? and chapter = ?', (f"%{book}%", chapter)).fetchall()
        else:
            found_verses[render_verse(result)] = cur.execute('select * from verses where book like ? and chapter = ? and verse between ? and ?', (f"%{book}%", chapter, start, stop)).fetchall()

    return {
        'plan': reading_plan_results,
        'verses': found_verses
    }


@app.route('/plan')
def get_plan():
    con = sqlite3.connect('bible.db')
    cur = con.cursor()
    return cur.execute('select * from reading_plan order by day').fetchall()
