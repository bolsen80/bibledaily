# Daily Bible Reading Plan Page

Simple application to keeps track and shows each day's Bible reading.

Reading plan from https://www.biblica.com
KJV bible text from https://gutenberg.org/cache/epub/10/pg10.txt

I also had this thought in my mind, because biblica.com's format for showing a daily reading has odd issues with caching pages I already read (coming back to a page the next day returns yesterday's reading), does tracking with Google Analytics, Facebook, etc. and is also trying to sell up their ministry with a blast of ads. I also want to use a good serif font for reading, since serif fonts make it easier to read on a screen.

# Build database

Run `python build_resources.py`.

# Start server

Run `yarn start`.
