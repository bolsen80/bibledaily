import { AppDispatch, AppState } from "./types";

export const initialState: AppState = {
    day: 1, plan: [], readingSelection: {}, view: 'readingSelection'
};

/**
 * Just a function that takes a state, an action and payload and does a thing..
 */
export const reducer = (state: AppState = initialState, action: AppDispatch) => {
    switch (action.type) {
        default:
            return state;
    }
}
