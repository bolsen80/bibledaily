import React from "react";
import { DispatchFunc } from './use-reducer-thunk';

// This captures the state of the application.
export type AppState = {
    day: Number,
    plan: Array<String>[],
    readingSelection: { [key: string]: string },
    view: 'readingSelection' | 'plan'
}

// This captures the list of actions that can be performed in a reducer.
export enum AppActionType {
    PlanRetrieved,
    SelectionRetrieved
};

// this is the form of a dispatch object
export type AppDispatch = {
    type: AppActionType;
    payload: any;
}


export type AppDispatchFunction = (dispatch: React.Dispatch<AppDispatch>) => Promise<void>;

// This captures the context that is passed into `React.createContext()`
export type AppContext = {
    state: AppState;
    dispatch: DispatchFunc<AppState, AppDispatch>;
}
