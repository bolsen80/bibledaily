import React, { createContext, useContext } from 'react';
import useReducerWithThunk from './use-reducer-thunk';
import { AppContext } from './types';
import { initialState } from './reducer';

type StateProviderProps = { reducer: (state: any, action: any) => any, initialState: any, children: React.ReactNode | React.ReactNode[] }

export const MainContext = createContext<AppContext>({
    state: initialState,
    dispatch: (_) => { }
});

// This wraps that context provider we made.
export const StateProvider = ({ reducer, initialState, children }: StateProviderProps) => {
    const [state, dispatch] = useReducerWithThunk(reducer, initialState);

    return (
        <MainContext.Provider value={{ state, dispatch }}>
            {children}
        </MainContext.Provider>
    );
};

export const useAppState = () => useContext(MainContext);
