const host = 'http://localhost:5000';

export async function getPlan(): Promise<unknown> {
    const result = await fetch(`${host}/plan`, { mode: 'cors' });
    return result.json();
}

/**
 * Take the plan data and batch verses together in the same day.
 * Example: [[0, 1, 'a'], [1, 1, 'b']] -> [[1, 'a', 'b']]
 *
 * Just assuming the data is sorted already. A pre-sorted list is quicker to scan through sequentially.
 */
export function batchPlanInDays(plans: any): any {
    const result: any[] = [[1]];
    let day = 1;
    for (const plan in plans) {
        if (day !== plans[plan][1]) {
            day++;
            result.push([day]);
        }

        let formattedVerse = `${plans[plan][2]} ${plans[plan][3]}`;
        if (plans[plan][4]) {
            formattedVerse += `:${plans[plan][4]}`;
        }
        if (plans[plan][5]) {
            formattedVerse += `-${plans[plan][5]}`;
        }
        result[result.length - 1].push(formattedVerse);
    }

    return result;
}

export async function getDailyVerse(day: Number): Promise<{ verses: { [key: string]: any } }> {
    const result = await fetch(`${host}/verses/${day}`, { mode: 'cors' });
    return result.json();
}

/**
 * Convert an array of verses into a paragraph of verses.
 */
export function getVerseData(verses: [number, string, number, number, string][]): string {
    const verse_text = []
    for (const verse in verses) {
        const [, , , , text] = verses[verse];
        verse_text.push(text);
    }
    return verse_text.join(" ");
}

/**
 * Return a structure of {"VerseSelection": "text"}
 */
export function getJustVerses(verses: { [key: string]: any }): { [key: string]: string } {
    // [7,"﻿Genesis",1,7,"And God made the firmament, and divided the waters which were under the firmament from the waters which were above the firmament: and it was so."]
    let selection: ReturnType<typeof getJustVerses> = {};
    for (const verse in verses) {
        selection[verse] = getVerseData(verses[verse]);
    }
    return selection;
}
