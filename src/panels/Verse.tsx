import React from 'react';
import './Verse.scss';

type VerseProps = { header: string, verses: string };

export default function Verse({ header, verses }: VerseProps) {
    return (
        <div>
            <div className="Verse-header">{header}</div>
            <div className="Verse-verses">{verses}</div>
        </div>
    )
}
