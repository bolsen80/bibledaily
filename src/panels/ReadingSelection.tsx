import React from 'react';
import { AppState } from '../reducer/types';

import Verse from './Verse';


export default function ReadingSelection({ readingSelection }: AppState) {
    let verseList: React.ReactElement[] = [];
    for (const selection in readingSelection) {
        verseList.push(
            <Verse header={selection} verses={readingSelection[selection]} key={selection} />
        );
    }


    return (
        <div className="ReadingSelection-Verses">
            {verseList}
        </div>
    );
}
