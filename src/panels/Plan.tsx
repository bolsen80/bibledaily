import React, { Key } from 'react';
import { Table } from 'react-bootstrap';
import { batchPlanInDays } from '../api';
import { AppState } from '../reducer/types';

export default function Plan({ plan, goToPlan }: Partial<AppState> & { goToPlan: Function }) {
    plan = plan || [];

    return (
        <div>
            <div className="Plan-Header">Plan</div>
            <Table striped hover>
                <thead>
                    <tr>
                        <th>Day</th>
                        <th>Verse</th>
                    </tr>
                </thead>

                <tbody>
                    {batchPlanInDays(plan).map((row: any) => (
                        <tr key={row[0] as Key}>
                            <td>
                                <a href="#" onClick={e => {
                                    e.preventDefault();
                                    goToPlan(row[0]);
                                    window.scrollTo({ top: 0 });
                                }}>{row[0]}</a>
                            </td>
                            {row.slice(1).map((verse: any) => <td key={verse as Key}>{verse}</td>)}
                        </tr>
                    ))}
                </tbody>
            </Table>
        </div>
    );
}
