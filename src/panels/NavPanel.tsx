import React, { useEffect, useState } from 'react';
import { Button, Form, Container } from 'react-bootstrap';
import { AppState } from '../reducer/types';
import './NavPanel.scss';

export default function NavPanel({ day, goToPlan, goToPlanListing }: Partial<AppState> & { goToPlan: Function, goToPlanListing: Function }) {
    const [state, setState] = useState(day || 0);

    useEffect(() => { setState(day ? day : 1); }, [day]);

    return (
        <Form className="NavPanel" onSubmit={e => {
            e.preventDefault();
            goToPlan(state);
        }}>
            <Container>
                <div className="NavPanel-Form">
                    <Form.Label>Day:</Form.Label>
                    <Form.Control
                        type="integer"
                        id="NavPanel-Input"
                        value={(state || "").toString()}
                        onChange={e => {
                            e.preventDefault();
                            setState(parseInt(e.target.value));
                        }}
                    />
                </div>
                <div className="NavPanel-Button">
                    <Button id="NavPanel-Btn" onClick={e => {
                        e.preventDefault();
                        goToPlan(state)
                    }}>Go</Button>
                </div>
            </Container>
            <div id="NavPanel-ViewingDay">{`Viewing Day: ${day}`}</div>
            <div>
                <a href="#" onClick={e => {
                    e.preventDefault();
                    goToPlanListing();
                }}>Reading Plan</a>
            </div>
        </Form>
    );
}
