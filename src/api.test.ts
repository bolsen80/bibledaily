import { batchPlanInDays } from "./api";

const fixture = [[1720, 1, "Genesis", 1, null, null], [1721, 1, "Genesis", 2, 1, 17], [1722, 1, "Matthew", 1, 1, 25], [1723, 1, "Psalm", 1, 1, 6], [1724, 2, "Genesis", 2, 18, 25], [1725, 2, "Genesis", 3, null, null], [1726, 2, "Genesis", 4, 1, 16], [1727, 2, "Matthew", 2, 1, 18], [1728, 2, "Psalm", 2, 1, 12], [1729, 3, "Genesis", 4, 17, 26], [1730, 3, "Genesis", 5, null, null], [1731, 3, "Genesis", 6, null, null], [1732, 3, "Matthew", 2, 19, 23], [1733, 3, "Matthew", 3, null, null], [1734, 3, "Psalm", 3, 1, 8], [1735, 4, "Genesis", 7, null, null], [1736, 4, "Genesis", 8, null, null], [1737, 4, "Genesis", 9, 1, 17]];

const fixtureFormatted = [
    [1, 'Genesis 1', 'Genesis 2:1-17', 'Matthew 1:1-25', 'Psalm 1:1-6'],
    [
        2,
        'Genesis 2:18-25',
        'Genesis 3',
        'Genesis 4:1-16',
        'Matthew 2:1-18',
        'Psalm 2:1-12'
    ],
    [
        3,
        'Genesis 4:17-26',
        'Genesis 5',
        'Genesis 6',
        'Matthew 2:19-23',
        'Matthew 3',
        'Psalm 3:1-8'
    ],
    [4, 'Genesis 7', 'Genesis 8', 'Genesis 9:1-17']
]



test('batchPlanInDays should render input correctly', () => {
    expect(batchPlanInDays(fixture)).toEqual(fixtureFormatted);
});
