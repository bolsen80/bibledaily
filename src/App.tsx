import './App.scss';

import React from 'react';
import { Row, Col, Container, ThemeProvider } from 'react-bootstrap';

import NavPanel from './panels/NavPanel';
import ReadingSelection from './panels/ReadingSelection';
import { AppState } from './reducer/types';
import { getDailyVerse, getJustVerses, getPlan } from './api';
import Plan from './panels/Plan';

class App extends React.Component {
    state: AppState;

    constructor(props = {}) {
        super(props);
        this.state = {
            view: 'readingSelection',
            day: 1,
            plan: [],
            readingSelection: {}
        };
    }

    loadSelection(day: Number) {
        this.setState({ ...this.state, day });
        new Promise<void>(async resolve => {
            const readingSelectionData = await getDailyVerse(day);
            const verses = readingSelectionData.verses;
            const readingSelection = getJustVerses(verses);
            this.setState({ ...this.state, readingSelection, view: 'readingSelection' });
            resolve();
        });
    }

    async loadPlan() {
        const plan = await getPlan();
        this.setState({ ...this.state, plan });
    }

    componentDidMount() {
        // Set day from LS
        const lsDay = localStorage.getItem("day") || "";
        const day = parseInt(lsDay) || this.state.day;
        Promise.all([
            this.loadSelection(day),
            this.loadPlan()
        ]);
    }

    componentWillUnmount() {
        // Nothing has to happen here.
    }

    goToPlan(day: Number) {
        this.loadSelection(day);
        localStorage.setItem("day", day.toString());
    }

    goToPlanListing() {
        this.setState({ ...this.state, view: 'plan' });
    }

    render() {
        return (
            <ThemeProvider breakpoints={['xxxl', 'xxl', 'xl', 'lg', 'md', 'sm', 'xs', 'xxs']} minBreakpoint="xxs">
                <div className="App">
                    <Container className="App-content" fluid>
                        <Row>
                            <Col xs={2} xxs={3}>
                                <NavPanel {...this.state} day={this.state.day}
                                    goToPlanListing={this.goToPlanListing.bind(this)}
                                    goToPlan={this.goToPlan.bind(this)} />
                            </Col>
                            <Col>
                                {this.state.view === 'readingSelection' && <ReadingSelection {...this.state} /> ||
                                    <Plan plan={this.state.plan} goToPlan={this.goToPlan.bind(this)} />}
                            </Col>
                        </Row>
                    </Container>
                </div>
            </ThemeProvider>
        );
    }
}

export default App;
