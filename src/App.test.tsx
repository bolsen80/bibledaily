import { act, fireEvent, render, screen } from '@testing-library/react';
import { unmountComponentAtNode } from 'react-dom';
import App from './App';
import NavPanel from './panels/NavPanel';

let container: HTMLElement | undefined | null;
beforeEach(() => {
    container = document.createElement('root');
    document.body.appendChild(container as HTMLElement);
});

afterEach(() => {
    unmountComponentAtNode(container as HTMLElement);
    container?.remove();
    container = null;
});

test('renders the navpanel', () => {
    render(<App />);
    const element = screen.getAllByText(/Day/i)[0];
    expect(element).toBeInTheDocument();
});

test('NavPanel basic check on clicking Go button', () => {
    const goToPlan = jest.fn();
    act(() => {
        render(<NavPanel day={3} goToPlan={goToPlan} goToPlanListing={() => { }} />);
    });
    const button = document.querySelector("#NavPanel-Btn");

    act(() => {
        button?.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    });

    expect(goToPlan).toBeCalledTimes(1);
});

test('NavPanel changes day state on clicking Go button', () => {
    render(<App />);

    const button = document.querySelector('#NavPanel-Btn');
    let input: HTMLInputElement = document.querySelector('#NavPanel-Input') as HTMLInputElement;
    act(() => {
        fireEvent.change(input, { target: { value: '10' } });
        fireEvent.click(button as Element);
    });

    expect(localStorage.getItem('day')).toEqual('10'); // side effect to changing the day is storing it in localstorage.

    input = document.querySelector('#NavPanel-Input') as HTMLInputElement;
    expect(input.value).toEqual("10");


    const viewDay = document.querySelector('#NavPanel-ViewingDay') as HTMLElement;
    expect(viewDay.innerHTML).toEqual("Viewing Day: 10"); // This occurs immediately because setting happens prior to the Promise
});
